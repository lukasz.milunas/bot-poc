package com.modivo.botpoc

import com.vaadin.flow.component.html.H1
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.Route
import io.weaviate.client.Config
import io.weaviate.client.WeaviateClient
import io.weaviate.client.v1.data.model.WeaviateObject
import io.weaviate.client.v1.graphql.query.argument.NearTextArgument
import io.weaviate.client.v1.graphql.query.fields.Field
import io.weaviate.client.v1.schema.model.WeaviateClass
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
class BotPocApplication

fun main(args: Array<String>) {
    runApplication<BotPocApplication>(*args)
}

@RestController
class WeaviateHelloWorld {

    private val weaviateClient = WeaviateClient(Config("http", "localhost:8000"))
    private val productClassName = "Product"
    private val productClazz = WeaviateClass.builder().className(productClassName).vectorizer("text2vec-transformers").build()
    private val schemaProduct = weaviateClient.schema()
        .classCreator()
        .withClass(productClazz)

    private val batchSize = 100
    private val responseLimit = 4

    init {
        weaviateClient.schema().allDeleter().run()
        schemaProduct.run()
    }

    @GetMapping("import-products")
    fun import() {
        importProducts()
    }

    @GetMapping("query-products/{query}")
    fun query(@PathVariable query: String) = weaviateClient.graphQL().get()
        .withClassName(productClassName)
        .withFields(
            Field.builder().name("name").build(),
            Field.builder().name("description").build(),
            Field.builder().name("_additional").build(),
            Field.builder().name("{distance}").build(),
        )
        .withNearText(NearTextArgument.builder().concepts(arrayOf(query)).build())
        .withLimit(responseLimit)
        .run()

    private fun importProducts() {
        val data = getData()
        val batcher = weaviateClient.batch().objectsBatcher()

        data.map { it.toWeaviateObject() }
            .chunked(batchSize)
            .map { batcher.withObjects(*it.toTypedArray()).run() }
            .forEach {
                it.result.forEach { println(it) }
                it.error?.let { println(it.messages) }
            }
    }

    private fun getData(): List<Product> = listOf(
        Product("1", "Buty z piórami czerwone", "kapcie z kompletu", "modivo,kobiety,kobiety/odziez,bielizna,komplety piżamowe"),
        Product("2", "Szpilki", "czerwone eleganckie buty typu szpilki", "modivo,kobiety,obouwie,szpilki"),
        Product("2", "Buty Noosa", "czerwone buty typu adidas", "modivo,mezczyzni,mezczyzni/sport,buty asfaltowe"),
    )

    private fun Product.toWeaviateObject(): WeaviateObject = WeaviateObject.builder()
        .className(productClassName)
        .properties(mapOf(
            "name" to name,
            "description" to description,
        )).build()

    data class Product(val id: String, val name: String, val description: String, val categories: String)

}

@Route("")
class MainView : VerticalLayout() {
    init {
        add(H1("Hello, world"))
    }
}
