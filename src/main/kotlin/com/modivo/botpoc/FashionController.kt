package com.modivo.botpoc

import org.springframework.web.bind.annotation.RestController

@RestController
class FashionController {
}

data class FashionObject(
    val categories: List<String>,
    val name: String,
    val description: String,
)